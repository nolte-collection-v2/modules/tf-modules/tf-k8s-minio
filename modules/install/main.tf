
locals {
  EXTRA_VALUES = {
  }
}


resource "helm_release" "release" {
  name       = "minio"
  repository = "https://helm.min.io/"
  chart      = "minio"
  version    = var.chart_version
  namespace  = var.namespace
  values = [
    yamlencode(local.EXTRA_VALUES),
    yamlencode(var.extra_values)
  ]
}