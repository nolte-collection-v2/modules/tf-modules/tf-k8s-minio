variable "namespace" {
  default = ""
}
variable "chart_version" {
  default = "8.0.4"
}

variable "extra_values" {
  default = {}
}
